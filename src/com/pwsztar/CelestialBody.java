package com.pwsztar;

public class CelestialBody {
  public double position_x;
  public double position_y;
  public double position_z;

  public CelestialBody(double position_x, double position_y, double position_z) {
    this.position_x = position_x;
    this.position_y = position_y;
    this.position_z = position_z;
  }
}
