package com.pwsztar;

public class DistanceCalculator {

  public double distanceBetweenPlanets(CelestialBody celestial1, CelestialBody celestial2) {
    return Math.sqrt(
        Math.pow(celestial1.position_x - celestial2.position_x, 2) +
        Math.pow(celestial1.position_y - celestial2.position_y, 2) +
        Math.pow(celestial1.position_z - celestial2.position_z, 2));
  }
}
