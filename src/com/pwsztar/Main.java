package com.pwsztar;

public class Main {
    public static void main(String[] args) {

        DistanceCalculator distanceCalculator = new DistanceCalculator();
        System.out.println(distanceCalculator.distanceBetweenPlanets(
            new CelestialBody(0.0,0.0,0.0),
            new CelestialBody(20.3, 100.3, 109.0)) + " mln km");
    }
}
